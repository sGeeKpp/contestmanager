﻿namespace Contest.Core.Repository.Exceptions
{
    /// <summary>
    /// Represent error when client ask unik item and several items was found.
    /// </summary>
    public class SeveralItemFoundException : RepositoryException
    { }
}
