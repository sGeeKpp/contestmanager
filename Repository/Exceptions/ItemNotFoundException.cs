﻿namespace Contest.Core.Repository.Exceptions
{
    /// <summary>
    /// Represent error when client ask unik item and no item was found.
    /// </summary>
    public class ItemNotFoundException : RepositoryException
    { }
}
