﻿using System;

namespace Contest.Core.Repository.Exceptions
{
    /// <summary>
    /// Represent generic errors occurs during repository treatment
    /// </summary>
    public class RepositoryException : Exception
    {
    }
}
