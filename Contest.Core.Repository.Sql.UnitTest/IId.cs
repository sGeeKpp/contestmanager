﻿using System;

namespace Contest.Core.Repository.Sql.UnitTest
{
    public interface IId
    {
        Guid Id { get; set; }
    }
}
