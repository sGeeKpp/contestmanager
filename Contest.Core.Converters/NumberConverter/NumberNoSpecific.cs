﻿namespace Contest.Core.Converters.NumberConverter
{
    public class NumberNoSpecific : Number
    {
        #region Properties

        public override string Pattern { get { return null; } }

        #endregion
    }
}