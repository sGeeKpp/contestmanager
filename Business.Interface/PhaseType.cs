using Contest.Core.Windows;

namespace Contest.Business
{
    public enum PhaseType
    {
        [Display("Qualification")]
        Qualification,

        [Display("Consolante")]
        Consoling,

        [Display("Principale")]
        Main
    }
}